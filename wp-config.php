<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'Fbreiseshuttle_wp' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'k:u,QKT5+c7Sp!+x[r3JGN:o^00I5OuQ)K44=AM=^[Et(]U]`-j{uj5NW`p7k@M{' );
define( 'SECURE_AUTH_KEY',  '5C^iPIELw}IkFr(Pqy1G/f.;.kZ;gEH)m{sT(VVInKh9}3L_Is2I![%PL+695O~5' );
define( 'LOGGED_IN_KEY',    'yxJc0T7J*M*r:,; |Idua>J$-3VY-eWWmTPY!o<<RPT.{aR!bJ*oMI/~HdY0^lzZ' );
define( 'NONCE_KEY',        'OLjzlMOj+w&tdQNG:.v1VlMOz^Ho]1O>aCa>/MvW**<>;]5?o+bugd7cQTc_QB6K' );
define( 'AUTH_SALT',        'bb2r*jY&/Itz8u<m6K+%y{=[eiCtk5zbWb}{|@_|.3_.9H_DiHQy^Aq>@mpWAJNs' );
define( 'SECURE_AUTH_SALT', 'C-5`-7VFYpT(EBPd?l5nZ8BzN?G)x.TjT;p-g3kJmagU#B8bfDu$CLpK+/QqfwK0' );
define( 'LOGGED_IN_SALT',   '=]OC3^0:%8kQaN-Ax!t[GYBRpZJ.D9}a~3s5s^;*E_emKzQ>8Sc?pApO/0|U&<0g' );
define( 'NONCE_SALT',       'c2d{~ZY;-F]>~Z~htzIJ`P%K)qOb~r1N4[f2L>U h18~XRm+>.WDd8m>E}&KrfzZ' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
