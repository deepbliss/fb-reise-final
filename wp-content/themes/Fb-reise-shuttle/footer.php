<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>

<footer class="footer-sec">

      <div class="bottom-footer container-fluid">
       <div class="container">        
         <div class="footer-copyright wow fadeIn">
           <div class="footer-content">
             <?php wp_nav_menu( array('menu' => 'Header-menu')); ?>
           
           </div>
           <div class="footer-designed"><p> <?php the_field("Copyright", 'option');?></p></div>
         
         </div>
        </div>
      
      </div>
   
</footer>
		

                  <script src="<?php echo get_template_directory_uri(); ?>/js/jquery-3.3.1.min.js"></script>
                    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.mmenu.all.js"></script>
                     <script src="<?php echo get_template_directory_uri(); ?>/js/wow.min.js"></script>
                <script src="<?php echo get_template_directory_uri(); ?>/js/owl.carousel.min.js"></script>
               <script>  
       $('.home-carousel').owlCarousel({
    loop:false,
    
 animateIn: 'fadeIn',
    responsiveClass:true,
    dots: false,
    responsive:{
        0:{
            items:1,
			 loop:false,
            nav:true
			
        },
        600:{
            items:1,
            nav:false,
			 loop:false
        },
        1000:{
            items:1,
            nav:true,
            loop:false
        }
    }
})
       
        </script> 
  <script>
      jQuery(document).ready(function( $ ) {
         $("#menu").mmenu({
            "extensions": [
              "effect-slide-menu",
			  "pageshadow"
            
            ],
        
         navbars:[ {
             position: 'top',
             content: ['prev', 'title', 'close']
         }
        
         ]
            
         });
      });
    </script>
    <script>
              new WOW().init();
              </script>
			  
    <script>
     
    var offset = 300,
        offset_opacity = 1200,
        scroll_top_duration = 700,
        $back_to_top = $('.cd-top');
    $(window).scroll(function() {
        ($(this).scrollTop() > offset) ? $back_to_top.addClass('cd-is-visible'): $back_to_top.removeClass('cd-is-visible cd-fade-out');
        if ($(this).scrollTop() > offset_opacity) {
            $back_to_top.addClass('cd-fade-out');
        }
    });
    $back_to_top.on('click', function(event) {
        event.preventDefault();
        $('body,html').animate({
            scrollTop: 0,
        }, scroll_top_duration);
    });
    $('a[href=#top]').click(function() {
        $('body,html').animate({
            scrollTop: 0
        }, 600);
        return false;
    });
	
       </script>          

 <script>
$(document).ready(function() {
  $(".mm-listview li").click(function(){
  $(this).parents('.mm-menu').removeClass('mm-menu_opened');
  $('html').removeClass('mm-wrapper_opened mm-wrapper_blocking mm-wrapper_background mm-wrapper_opening');   
}) 
   $(".menu-btn a").click(function(){
  $(this).parents('body').find('.mm-menu').addClass('mm-menu_opened');
  $('html').addClass('mm-wrapper_opened mm-wrapper_blocking mm-wrapper_background mm-wrapper_opening');
}) 
});
</script>



<?php wp_footer(); ?>

</body>
</html>

