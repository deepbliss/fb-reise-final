<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

  <section class="banner-sec">
   <div class="primary">
        <div class="home-banner container-fluid">
        <?php home_banner() ?>
         
         
         </div>
        </div>
        

        </section>  
          <a href="#our-service"></a> 
            <section class="aboutus-sec container-fluid" id="our-service">
         <div class="about-main-sec" id="about-us">
           <div class="container">

           	<?php 
if( have_rows('inner_content') ): 
while ( have_rows('inner_content') ) : the_row();?>
 <div class="item-page-sec">
        
           <div class="left-sec">
           <img src="<?php the_sub_field("about_us_image"); ?>" >
           
           
           </div>
           <div class="right-sec">
	            <div class="aboutus-content-sec">
	             <div class="aboutus-tittle">
	               <h2 class="main-title"><?php the_sub_field("about_us_heading"); ?></h2>
	             </div>
	             <div class="aboutus-content">
	              <?php the_sub_field("about_us_text"); ?>
	           
	           </div>
	           </div>           
             </div>
</div>		

             <?php endwhile; 
           endif; ?>
           </div>
           </div>
        </section>
<a href="#kontakt"></a> 
      <section class="content-sec">
      	     
      <div class="contact-outer" id="kontakt">
        <div class="container">
       <div class="map-outer">
         
          <div class="map-right">
             <div class="contact-tittle">
               <h2 class="main-title title-contact"> <?php the_field("contact_heading");?></h2>
             </div>
            <div class="contact-detail">
            <?php the_field("contact_us_content");?>
              
            </div>
          </div>
       
       <div class="map-div"> <?php the_field("map_iframe");?></div>
       
       
       </div>
          
        </div>
      
      </div>



      </section>



<?php
get_footer();
