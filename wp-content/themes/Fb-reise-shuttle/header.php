<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
   <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="icon" type="image/ico" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico">
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/stylesheet.css">
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/mediaquery.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/owl.carousel.min.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/animate.css">
      
  <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i" rel="stylesheet"> 

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>


<div class="wrapper container-fluid">
        <header class="header container-fluid">
             
            
               
                 <div class="container">
              
                    <div class="header-lft">
                        <div class="menu-btn"><a href="#menu"><img src="<?php echo get_template_directory_uri(); ?>/images/menu.png" alt="menu" title="menu"></a></div> 
                      <div class="logo"> <figure><a href="<?php echo site_url(); ?>"> <img src="<?php the_field("logo", 'option');?>" alt="Taplinsautomotive" title="Taplinsautomotive"></a></figure></div>
                  
                    </div>
					
					<div class="header-right">
                      <div class="h-top-rgt">
                        <div class="text"><p><?php the_field("address", 'option');?></p></div> 
                       
                        
                        
                         <div class="call">
                     <a href="tel:<?php the_field("moblie_phone", 'option');?>"><?php the_field("phone", 'option');?></a>
                  </div>
                        
                      
                      </div>
					  <div class="h-btm-rgt">
                       <?php wp_nav_menu( array('menu' => 'Header-menu')); ?>
                      
                      </div>
            <div class="mob-menu" style="display: none;">
              <nav id="menu">
                 <?php wp_nav_menu( array('menu' => 'Header-menu')); ?>
              </nav>
            </div>
					
					</div>
             </div>
         
			 
			 </header>		
